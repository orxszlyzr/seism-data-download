#-*- coding: utf-8 -*-

def group(data, compfunc, tolerance):
    lastitem = None
    result = []
    thegroup = []
 
    for each in data:
        if lastitem == None:
            lastitem = each
            thegroup.append(each)
        else:
            if compfunc(each, lastitem) >= tolerance:
                thegroup.append(each)
            else:
                result.append(thegroup)
                thegroup = [each,]
            lastitem = each
    result.append(thegroup)
    return result

def eval_earthquake(e1,e2):
    if e1['SRC'] == e2['SRC']:
        return 0
    m_diff = abs(e1['M'] - e2['M'])
    time_diff = abs(e1['TIME'] - e2['TIME'])
    long_diff = abs(e1['LONG'] - e2['LONG'])
    lat_diff = abs(e1['LAT'] - e2['LAT'])

    m = ((m_diff < 0.3) and 100) or 0
    time = ((time_diff < 15) and 100) or 0
    long = ((long_diff < 0.5) and 100) or 0
    lat = ((lat_diff < 0.5) and 100) or 0

    return m * 0.1 + time * 0.4 + long * 0.25 + lat * 0.25
