# -*- coding: utf-8 -*-

def hash(i):
    return "%d-%8.3f-%8.3f-%2.1f" % (i['TIME'],i['LONG'],i['LAT'],i['M'])
