#-*- coding: utf-8 -*-
from operator import itemgetter

def sort(inputlist,keyname,rev=False):
    return sorted(inputlist, key=itemgetter('TIME'), reverse=rev)
