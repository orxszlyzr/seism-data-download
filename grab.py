# -*- coding: utf-8 -*-
import os,sys

from operator import itemgetter

import analyze.sort, analyze.hash
from analyze.group import *
import sources.usgs, sources.ceic, sources.jopens


SRCS = {'CEIC'  : [sources.ceic.data,],
        'USGS'  : [sources.usgs.data,],
        'JOPENS': [sources.jopens.data,],
       }
BASEPATH = os.path.realpath(os.path.dirname(sys.argv[0]))

DEBUG = False
if not DEBUG:
    data = []
    for key in SRCS:
        print 'Downloading data from %s...' % key
        try:
            gotd = SRCS[key][0]().result
            print ' - Retrived %d earthquake record(s).' % len(gotd)
            data += gotd
        except:
            print ' - Error occured. No data retrived.'
    open(os.path.join(BASEPATH,'last_raw_data'),'w+').write(repr(data))
else:
    data = eval(open(os.path.join(BASEPATH,'last_raw_data'),'r').read())

data = analyze.sort.sort(data, 'TIME', True)

grouped = group(data, eval_earthquake, 70)

open(os.path.join(BASEPATH,'latest_retrived'),'w+').write(repr(grouped))
