# -*- coding: utf-8 -*-
import time,subprocess,os,sys

BASEPATH = os.path.realpath(os.path.dirname(sys.argv[0]))

def osd(message,desc=None,level=0):
    global BASEPATH

    if level in range(0,5):
        notifile = ['notify.mp3','caution.mp3','warning.mp3','alarm.mp3','danger.mp3'][level]
    else:
        return

    ns = subprocess.Popen(['mpg123','-q',os.path.join(BASEPATH,'alarms',notifile)])
    for i in range(0,3):
        osd_write(message,450.0)
    if desc != None:
        ns = subprocess.Popen(['mpg123','-q',os.path.join(BASEPATH,'alarms','notify.mp3')])
        for key in desc:
            osd_write(key,1500.0)
    
def osd_write(message,timed=450.0):
    ps = subprocess.Popen('gnome-osd-client -fs',shell=True,stdin=subprocess.PIPE)
    ps.stdin.write("""<message id='test' hide_timeout='%d' osd_vposition='top' osd_halignment='center'>\n\n<span foreground='#FF0000'>%s</span></message>""" % (timed,message))
    ps.communicate()
    time.sleep((timed + 150) / 1000)

if __name__ == '__main__':
    import sys
    level = int(sys.argv[1])
    osd('北京时间2012年12月21日 美国黄石发生7级地震',None,level)
