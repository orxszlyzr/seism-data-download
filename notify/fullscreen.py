# -*- coding: utf-8 -*-

import os, subprocess, sys, math, time
import PIL.Image, PIL.ImageTk, PIL.ImageOps, PIL.ImageEnhance
from Tkinter import *

import lsb

BASEPATH = os.path.realpath(os.path.dirname(sys.argv[0]))

# Scales, how many kilometers in a Baidu Map a fullscreen width can display.
# Tested under 1366x768.
BAIDU_MAP_SCALES = {  99999:2,                  10000:4, 7812:5,
                      4000:6,  2000:7,  1000:8,   500:9,  250:10,
                       125:11, 62.5:12, 31.2:13, 15.6:14, 7.8:15,}
MARKERS = {(5,2):'http://d.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=1ac3c6a9cc11728b342d8826f8ccf8bf/bf096b63f6246b60c7f0fb4ceaf81a4c510fa208.jpg',
           (5,4):'http://f.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=4786aa80d53f8794d7ff4c2ae22b3585/d62a6059252dd42afdf74893023b5bb5c9eab808.jpg',
           (5,6):'http://a.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=b246a53bc995d143de76e02743c0b973/902397dda144ad34a963cebfd1a20cf431ad852e.jpg',
           (5,8):'http://h.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=5d0f527b8ad4b31cf43c90bfb7e61c0e/f2deb48f8c5494eee7fd46212cf5e0fe99257e2e.jpg',
           (6,4):'http://e.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=974ae2590eb30f24319ae807f8a5ea32/b17eca8065380cd77de0ebd5a044ad3459828108.jpg',
           (6,6):'http://a.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=a26cd8074afbfbedd859327b48c0cc47/30adcbef76094b36546c52d5a2cc7cd98d109d08.jpg',
           (6,8):'http://a.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=30182d35279759ee4e5064cf82cb7867/a9d3fd1f4134970a08ac591794cad1c8a7865d2e.jpg',
           (7,6):'http://e.hiphotos.baidu.com/album/s%3D900%3Bq%3D90/sign=b5c530006f061d9579463b384bcf7bec/3b292df5e0fe9925a0f5206835a85edf8db17108.jpg',
           (7,8):'http://c.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=d5a697f01b4c510faac4e61e50691e5c/fd039245d688d43fd576d8ca7c1ed21b0ef43b2e.jpg',
           (3,5):'http://g.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=debeb68864380cd7e21ea6e991749645/43a7d933c895d143c38bad1b72f082025aaf070c.jpg',
           (3,7):'http://b.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=6a00c8878d5494ee83220b1d1dc5db8f/8601a18b87d6277f07f17b0029381f30e924fc23.jpg',
           (3,8):'http://c.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=e3e3ebd5a044ad342abf8383e0923785/ac4bd11373f08202286fd8074afbfbedab641b0c.jpg',
           (4,1):'http://h.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=8424fe83a71ea8d38e227000a73a0b3f/503d269759ee3d6de225cef342166d224f4ade23.jpg',
           (4,2):'http://a.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=3a55da16a9d3fd1f3209a63e007e1e6e/279759ee3d6d55fb0a253f1e6c224f4a20a4dd23.jpg',
           (4,3):'http://a.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=403ee52dbd315c6047956febbd81f062/1e30e924b899a901ecc8120e1c950a7b0208f50d.jpg',
           (4,5):'http://a.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=5514adc0a686c9170c03563df90d4bbe/21a4462309f7905258fc0e060df3d7ca7bcbd523.jpg',
           (4,7):'http://b.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=a0e3a15c574e9258a23482eaacb2ea29/7af40ad162d9f2d3feec6dd7a8ec8a136327cc0d.jpg',
           (4,8):'http://e.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=bbfe1b01c83d70cf48faae09c8ecea71/9922720e0cf3d7cabf0b764ef31fbe096b63a90d.jpg',
           (3,1):'http://a.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=158470df902397ddd2799c0069b289c6/c995d143ad4bd113034cfe0a5bafa40f4bfb0500.jpg',
           (3,2):'http://g.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=d21352d5a2cc7cd9fe2d30dd09311a4e/aec379310a55b3199578cb4d42a98226cffc1700.jpg',
           (3,3):'http://c.hiphotos.baidu.com/album/s%3D1400%3Bq%3D90/sign=e39febd5a044ad342abf8383e0923785/ac4bd11373f082022813d8074afbfbedab641b00.jpg',
           }

class fullscreen:
    def __init__(self,monitorCenter,quakeInfo):
        self.root = Tk()
        self.qinfo = quakeInfo
        self.mcenter = monitorCenter
        self.initializeWidgets()

    def strftime(self, timestamp):
        t = time.gmtime(timestamp - time.timezone)
        return time.strftime('%Y-%m-%d %H:%M:%S', t)

    def getAngle(self, postpl):
        def getStr(i):
            i = abs(i)
            d = int(i)
            i -= d
            i *= 60.0
            m = int(i)
            s = (i - m) * 60.0
            return "%d*%d'%d\"" % (d,m,s)
        return (getStr(postpl[0]) + ((postpl[0]>0 and 'E') or 'W'),
                getStr(postpl[1]) + ((postpl[1]>0 and 'N') or 'S'))

    def __constructMessageFrame(self,root):
        ret = Frame(root, bg="Black")
        ret.labelItems = {}
        ret.messages = {}
        def addDesc(index,title,value):
            labelConf = {'fg':'#0A0', 'bg':'Black', 'font':'Courier 12 bold', 'justify':LEFT}
            packConf = {'anchor':W}
            if len(ret.labelItems) > 0:
                title = '\n' + title
            ret.labelItems[index] = Label(ret, text=title, **labelConf)
            ret.messages[index] = Label(ret, text=value, **labelConf)
            ret.labelItems[index].pack(**packConf)
            ret.messages[index].pack(**packConf)

        addDesc('current_time',u'当前时刻','')
        addDesc('quake_time',u'发震时刻', self.strftime(self.qinfo['TIME']))
        addDesc('quake_mag',u'震级（单位）', "%s(%s)" % (self.qinfo['M'], self.qinfo['MU'].upper()))
        addDesc('position',u'震中坐标','%s\n%s' % self.getAngle((self.qinfo['LONG'],self.qinfo['LAT'])))
        addDesc('position',u'深度', '%s KM' % self.qinfo['DEPTH'])
        addDesc('position',u'数据来源', self.qinfo['SRC'])

        return ret

    def _constructBody(self, root):
        ret = Frame(root, bg="Black")

        qC = (self.qinfo['LONG'], self.qinfo['LAT'])
        qM = self.qinfo['M']
        self.loadMap(self.mcenter,qC,qM)
        self.mapbox = Label(ret, image=self.mapimg, bg="Black")
        self.mapbox.grid(row=0,column=0)

        self.messageFrame = self.__constructMessageFrame(ret)
        self.messageFrame.grid(row=0,column=1,sticky=N+E+W+S,padx=10)

        return ret

    def initializeWidgets(self):
        self.w, self.h = self.root.winfo_screenwidth(), self.root.winfo_screenheight()
        self.root.overrideredirect(1)
        self.root.geometry("%dx%d+0+0" % (self.w, self.h))
        self.root.configure(background='black')
        self.root.bind('<Escape>', lambda e: e.widget.quit())

        self.title = Label(self.root, text='地震报告', bg='Black', fg='#0A0',
                           font='Courier 40 bold')
        self.title.pack(fill=X,pady=30)

        self.body = self._constructBody(self.root)
        self.body.pack()

        def onQuitbuttonClicked():
            self.root.quit()
        self.quitbutton = Button(self.root)
        self.quitbutton['text'] = 'QUIT'
        self.quitbutton['command'] = onQuitbuttonClicked        
        self.quitbutton.pack()

    def _getAffectingRadius(self, m):
        if m <= 2:
            return 5
        if m <= 3:
            return 10
        if m <= 4:
            return 40
        if m <= 5:
            return 100
        if m <= 6:
            return 250
        if m <= 7:
            return 500
        else:
            return 1000
    def _getMarkerURL(self, m, zoom):
        if m > 7:
            m = 7
        if m < 3:
            m = 3
        m = int(math.floor(m))
        zoom = int(zoom)
        if zoom >= 12:
            s = 8
        elif zoom == 6:
            if m <= 3:
                s = 1
            elif m >= 7:
                s = 8
            else:
                s = [1, 4, 6][m - 4]
        elif zoom == 7:
            if m <= 3:
                s = 1
            elif m >= 7:
                s = 8
            else:
                s = [2, 6, 8][m - 4]
        elif zoom == 8:
            if m <= 3:
                s = 2
            elif m >= 5:
                s = 8
            else:
                s = 3
        elif zoom == 11:
            if m <=3:
                s = 7
            else:
                s = 8
        elif zoom == 10:
            if m <= 3:
                s = 5
            elif m >= 5:
                s = 8
            else:
                s = 7
        elif zoom == 9:
            if m <= 3:
                s = 3
            elif m >= 5:
                s = 8
            else:
                s = 5
        else:
            if m <= 3:
                s = 1
            elif m >= 7:
                s = 6
            else:
                s = [1, 2, 4][m - 4]
        if s > 6:
            s = 6
        return MARKERS[(m, s)]
    def loadMap(self, monitorCenter, quakeCenter, quakeMagnitude):
        map_width_factor = min(0.9, 990.0 / self.w) # of full screen width
        map_height_factor = min(0.7, 990.0 / self.h) # of full screen height

        # Find out where to center the map and zoom level
        mapCenter = ( (monitorCenter[0] + quakeCenter[0]) * 0.5, (monitorCenter[1] + quakeCenter[1]) * 0.5 )
        affectingRadius = self._getAffectingRadius(quakeMagnitude)
        _mapCenter = '%f,%f' % (mapCenter[0], mapCenter[1])
        _quakeCenter = '%f,%f' % (quakeCenter[0],quakeCenter[1])

        factor1 = math.pi / 180.0
        factor2 = factor1 * 6378
        distanceX = abs(monitorCenter[0] - quakeCenter[0]) * factor2 * \
                    math.cos( min(abs(monitorCenter[1]), abs(quakeCenter[1])) * factor1)
        distanceY = abs(monitorCenter[1] - quakeCenter[1]) * factor2
        requiredWidth = distanceX + 2.5 * affectingRadius
        requiredHeight = distanceY + 2.5 * affectingRadius

        dictWidths, dictHeights = {}, {}
        for each in BAIDU_MAP_SCALES:
            downScaleFactor = 0.60
            dictWidths[map_width_factor * each * downScaleFactor - requiredWidth] = BAIDU_MAP_SCALES[each]
            dictHeights[map_height_factor * each * downScaleFactor - requiredHeight] = BAIDU_MAP_SCALES[each]
        choosenScale = min(dictWidths[min([i for i in dictWidths if i > 0])],
                           dictHeights[min([i for i in dictHeights if i > 0])],)

        markerURL = self._getMarkerURL(quakeMagnitude, choosenScale)
        URL = 'http://api.map.baidu.com/staticimage?width=%f&height=%f&center=%s&zoom=%s' % \
              (map_width_factor * self.w, map_height_factor * self.h, _mapCenter, choosenScale)
        URL += '&markers=%s&markerStyles=-1,%s,-1' % (_quakeCenter, markerURL)
        self.mapFile = os.path.join(BASEPATH, 'fullscreen_notifier_map.temp')
        subprocess.call(['wget','-O',self.mapFile,URL])

        self._img = PIL.Image.open(self.mapFile).convert('RGB')
        r,g,b = self._img.split()
        bg = PIL.ImageOps.invert(r)
        self._img = PIL.ImageOps.invert(self._img)
        r,g,b = self._img.split()
        marklayer = lsb.getXor(lsb.getlayer(r,7),lsb.getlayer(g,7)).convert('L')
        b = PIL.Image.new('L',r.size,0)
        self._img = PIL.Image.merge('RGB',(marklayer,bg,b))

        matrix = (1,0,0,0,
                  0,1,1,0,
                  0,0,0,0)

        self._img = self._img.convert('RGB',matrix)
#        self._img = PIL.ImageEnhance.Brightness(self._img).enhance(0.5)

        self.mapimg = PIL.ImageTk.PhotoImage(self._img)
        os.remove(self.mapFile)

    def doShortAfter(self):
        self.messageFrame.messages['current_time']['text'] = self.strftime(time.time())
        self.root.after(100, self.doShortAfter)

    def doLongAfter(self):
        subprocess.call(['xscreensaver-command','-deactivate'])
        self.root.after(15000, self.doLongAfter)

    def show(self):
        self.root.focus_set()
        self.doShortAfter()
        self.doLongAfter()
        self.root.mainloop()

if __name__ == '__main__':
    quakeInfo = {'M':7.3, 'MU':'ml', 'SRC':'CEIC', 'LAT': 40.7, 'LONG': 122.68, 'DEPTH': 20, 'TIME': 160745760 }
    f = fullscreen( (123.87,42.31), quakeInfo )
    f.show()
