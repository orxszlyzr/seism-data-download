#!/usr/bin/python
# -*- coding: utf-8 -*-

import Image
def getlayer(srcLimg,mask):
    s = list(srcLimg.getdata())
    r = ''
    a = chr(255)
    b = chr(0)

    testlayer = mask

    for i in s:
        if i > 210:
            r += a
        else:
            r += b
    ret = Image.fromstring('L',srcLimg.size,r)
    return(ret)

def getXor(img1,img2):
    s1 = list(img1.getdata())
    s2 = list(img2.getdata())
    r = [] 
    for i in xrange(len(s1)):
        r += chr(s1[i] ^ s2[i])
    ret = Image.fromstring('L',img1.size,''.join(r))
    return ret
