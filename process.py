# -*- coding: utf-8 -*-

import os,sys,shelve,time

import notify.fullscreen
import analyze.hash

BASEPATH = os.path.realpath(os.path.dirname(sys.argv[0]))

grouped = eval(open(os.path.join(BASEPATH,'latest_retrived'),'r').read())
sh = shelve.open(os.path.join(BASEPATH,'data_base'),writeback=True)

if not sh.has_key('hashes'):
    sh['hashes'] = {}
if not sh.has_key('data'):
    sh['data'] = {}

alerts = []
latest_time = 0
new_reg = 0
for each in grouped:
    registered = False
    for i in each:
        h = analyze.hash.hash(i)
        latest_time = max(latest_time, i['TIME'])
        if sh['hashes'].has_key(h):
            registered = True
            break
    if not registered:
        hcmn = analyze.hash.hash(each[0])
        sh['data'][hcmn] = i
        new_reg += 1
        for i in each:
            h = analyze.hash.hash(i)
            sh['hashes'][h] = hcmn
        if i['M'] >= 3 and (time.time() - i['TIME'] < 86400):
            alerts.append(i)

print "Registered %d new items." % new_reg

for each in alerts:
    f = notify.fullscreen.fullscreen((123.87,42.31),each)
    f.show()
