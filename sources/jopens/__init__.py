# -*- coding: utf-8 -*-

import time
import urllib

from BeautifulSoup import BeautifulSoup

PATH = 'http://bulletin.gddsn.org.cn/seisbulletin/main.seam'

class data:
    def __init__(self,page=False):
        if not page:
            page = urllib.urlopen(PATH)
            self.soup = BeautifulSoup(page)
        else:
            self.soup = BeautifulSoup(page)
        tablines = self.soup.findAll(self._isWantedTr)[1:]
        self.result = []
        for each in tablines:
            data = [i.string for i in each('td', limit=7)]
            _m = data[6][0:-4]
            _mu = data[6][-3:-1]
            self.result.append({'M':float(_m),
                                'MU':_mu.lower().strip(),
                                'SRC':'JOPENS',
                                'LAT': float(data[3]),
                                'LONG': float(data[4]),
                                'DEPTH': float(data[5]),
                                'TIME': self.normalizeTime(data[2]),
                              })

    def _isWantedTr(self,tag):
        if not tag.name == 'tr':
            return False
        tds = tag.findAll('td')
        if len(tds) != 8:
            return False
        return True

    def normalizeTime(self,timestr):
        return time.mktime(time.strptime(timestr.strip()[0:19], "%Y-%m-%d %H:%M:%S")) - 28800

if __name__ == '__main__':
#    d = data(open('test.file','r').read())
    d = data()
    for each in d.result:
        print each
