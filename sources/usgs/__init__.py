# -*- coding: utf-8 -*-

import os
import sys
import time
import urllib

URL = 'http://earthquake.usgs.gov/earthquakes/feed/csv/all/'
BASEPATH = os.path.realpath(os.path.dirname(sys.argv[0]))
LASTREC = os.path.join(BASEPATH,'last_request.usgs')

class data:
    def __init__(self,page=False):
        if not page:
            diff = time.time() - self.getLastRequest()
            if diff < 3600:
                url = URL + 'hour'
            elif diff < 86400:
                url = URL + 'day'
            else:
                url = URL + 'week'
            print ' - using URL: %s' % url

            page = urllib.urlopen(url)
            tablines = page.readlines()
        else:
            tablines = page.split('\n')
        self.result = []
        for each in tablines[1:]:
            data = each.split(',')
            if len(data) < 6:
                continue
            try:
                self.result.append({'M':float(data[4]),
                                    'MU':data[5].lower().strip(),
                                    'SRC':'USGS',
                                    'LAT': float(data[1]),
                                    'LONG': float(data[2]),
                                    'DEPTH': (lambda x: x and float(x) or False)(data[3]),
                                    'TIME': self.normalizeTime(data[0]),
                                  })
            except:
                print 'Fatal:' + repr(data)

        self.setLastRequest(repr(self.result))
    def setLastRequest(self,data):
        open(LASTREC,'w+').write(data)

    def getLastRequest(self):
        if not os.path.isfile(LASTREC):
            return 0
        return os.path.getmtime(LASTREC)

    def normalizeTime(self, timestr):        
        return time.mktime(time.strptime(timestr.strip()[0:19], "%Y-%m-%dT%H:%M:%S"))

if __name__ == '__main__':
    d = data()
    for each in d.result:
        print each
